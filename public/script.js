
$(function () {

    $.get('/api/places', (data, status) => {
        if (status == 'success') {
            $.each(data, function (key, value) {
                $('#origin,#destination')
                    .append($("<option></option>")
                        .attr("value", value.id)
                        .text(value.name));
            });
        }
    });

    $("#searchForm").on('submit', (e) => {

        const formData = $("#searchForm").serializeArray();
        if (formData[1].value == 'null') {
            alert('Please select origin');
            return false;
        } else if (formData[2].value == 'null1') {
            alert('Please select destination');
            return false;
        } else if (formData[1].value == formData[2].value) {
            alert('Origin and destination cannot be same');
            return false;
        }

        $(".card-header").html("");
        const origin = $("#origin option:selected").text();
        const destination = $("#destination option:selected").text();

        let topHtml = ` ${origin} > ${destination}`;
        $(".card-header").append(topHtml);
        //decide which api to call
        const url = formData[0].value == 0 ? "/api/oneway" : "/api/return";

        // make api call to fecth data

        $("#errors").html('');
        $.ajax({
            url: url,
            type: "get", //send it through get method
            data: formData,
            success: (resp) => {
                $("#results").html('');
                if (formData[0].value == 0) {
                    oneWaySuccess(resp);
                } else {
                    returnSuccess(resp);
                }
            },
            error: function (xhr, status) {
                let div = '';
                $.each(xhr.responseJSON.errors, (key, value) => {
                    div = `
                    <div class="alert alert-warning">
                    <strong>Validation!</strong> ${value.msg}.
                    </div>
                    `;
                    $("#errors").append(div);
                });
            }
        });

        return false;
    });




});


function oneWaySuccess(response) {
    if (response.length > 0) {
        let div = '';
        $.each(response, (key, value) => {
            div = `
            <div class="card">
            <div class="card-body">
                <h3 class="card-title"><b>Rs. ${value.price}</b></h3>
                <h4 class="card-text">${value.airline}</h4>
                <p class="card-text">${value.airlineCode}-${value.flightNumber}</p>
                <p class="card-text">${new Date(value.origin).toDateString()} > ${new Date(value.destination).toDateString()}</p>
                <p class="card-text">Arrive:${value.arrival} | Departure: ${value.departure}</p>
                <p class="card-text">Duration : ${value.duration}</p>
                <div class="float-right">
                    <a href="#" class="btn btn-primary">Book Now</a></div>
                </div>
            </div>
          </div>
        </div>
            `;
            $("#results").append(div);
        })
    } else {
        $("#results").append("<h2>No records found!!</h2>")
    }
}

function returnSuccess(response) {
    debugger;
    console.log(response);

    if (response.length > 0) {
        let div = '';
        $.each(response, (key, value) => {
            div = `
            <div class="card">
                <div class="card-body"
                <h2 class="card-title"><b>Rs. ${value.totalPrice}</b></h3>
                <div class="row>
                <div>
                <h6 class="card-subtitle mb-2 text-muted">Go</h6>
                    <p class="card-text">${value.go.airline}</p>
                    <p class="card-text">${value.go.airlineCode}-${value.go.flightNumber}</p>
                    <p class="card-text">${value.go.origin} > ${value.go.destination}</p>
                    <p class="card-text">Arrive:${value.go.arrival} | Departure: ${value.go.departure}</p>
                    <p class="card-text">Duration : ${value.go.duration}</p>
                </div>
                <hr/>
                <div>
                <h6 class="card-subtitle mb-2 text-muted">Return</h6>
                    <p class="card-text">${value.back.airline}</p>
                    <p class="card-text">${value.back.airlineCode}-${value.back.flightNumber}</p>
                    <p class="card-text">${value.back.origin} > ${value.back.destination}</p>
                    <p class="card-text">Arrive:${value.back.arrival} | Departure: ${value.back.departure}</p>
                    <p class="card-text">Duration : ${value.back.duration}</p>
                </div>
                <div class="float-right">
                <a href="#" class="btn btn-primary">Book Now</a></div>
                </div>
                </div>
            </div>`;
            $("#results").append(div);
        })
    } else {
        $("#results").append("<h2>No records found!!</h2>")
    }
}

function toggle(p) {
    if (p == true) {
        $("#returnDiv").show();
    } else {
        $("#returnDiv").hide();
    }
}