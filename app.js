const express = require('express');
const bodyParser = require('body-parser');
const low = require('lowdb');
const FileAsync = require('lowdb/adapters/FileAsync');
const { validationResult } = require('express-validator/check');
const join = require('path').join;
const { validationArray, returnTicketValidation } = require('./schemaValidation');

const app = express();
app.use(bodyParser.json());
app.use(express.static('public'));

const adapter = new FileAsync('data.json');


low(adapter).then((db) => {
    /**
     * @description : loads index.html page
     */
    app.get('/', (req, res) => {
        res.sendFile(join(__dirname, 'public/index.html'))
    });

    /**
     * @description : Get search result for one way journey
     * @param {string} : origin
     * @param {string} : destinnation
     * @param {date} : deptDate
     * @param {number} :Number of passengers
     */
    app.get('/api/oneway', validationArray, (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
        }

        search(req.query)
            .then((resp) => res.json(resp));
    })

    /**
     * @description : Get search result for return journey
     * @param {string} : origin
     * @param {string} : destinnation
     * @param {date} : departure
     * @param {date} : return
     * @param {number} :Number of passengers
     */
    app.get('/api/return', returnTicketValidation, (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
        }

        search(req.query)
            .then((resp1) => {
                let temp = req.query.origin;
                req.query.origin = req.query.destination;
                req.query.destination = temp;
                req.query.departure = req.query.returnDate;
                search(req.query)
                    .then((resp2) => {
                        let op = [];
                        if (resp1.length != 0 || resp2.length != 0) {
                            resp1.forEach(el1 => {
                                resp2.forEach((el2) => {
                                    op.push({
                                        totalPrice: el1.price + el2.price,
                                        go: el1,
                                        back: el2
                                    });
                                })
                            });
                        }
                        res.json(op);
                    })
            });
    })

    app.get('/api/places', (req, res) => {
        const places = db.get('places').value();
        res.json(places);
    });

    const search = (p) => {
        return new Promise((resolve, reject) => {
            const data = db
                .get('flights')
                .filter((v) => {
                    const dbDate = new Date(v.departure).setHours(0, 0, 0, 0);
                    const paramDate = new Date(p.departure).setHours(0, 0, 0, 0);
                    return v.origin == p.origin &&
                        v.destination == p.destination &&
                        v.availableSeats > p.availableSeats &&
                        dbDate == paramDate
                })
                .map((v) => {
                    v.departure = new Date(v.departure).toLocaleString();
                    v.arrival = new Date(v.arrival).toLocaleString();
                    return v;
                })
                .value()

            resolve(data);
        });
    }

}).then(() => {
    app.listen(3001, () => console.log('listeing to port 3001!!'));

});






