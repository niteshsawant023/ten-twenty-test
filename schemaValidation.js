const { check } = require('express-validator/check');

let validationArray = [
    check('origin')
        .trim()
        .not()
        .isEmpty()
        .withMessage('Please select journey origin')
        .custom((val, { req }) => {
            return val == req.query.destination ? false : true;
        })
        .withMessage('Origin and Destination cannot be same'),
    check('destination')
        .trim()
        .not()
        .isEmpty()
        .withMessage('Please select journey destination'),
    check('departure')
        .isISO8601()
        .withMessage('Date passed is invalid')
        .custom((date) => {
            const today = new Date().setHours(0, 0, 0, 0);
            const paramDate = new Date(date).setHours(0, 0, 0, 0);
            return paramDate < today ? false : true;
        })
        .withMessage('Cannot select past date'),
    check('availableSeats')
        .isInt({ min: 1 })
        .withMessage('Number of passengers should be greater than 1'),
    check('returnDate')

];

let returnTicketValidation = validationArray.slice();
returnTicketValidation.push(check('returnDate')
    .isISO8601()
    .withMessage('Date passed is invalid')
    .custom((date) => {
        const today = new Date().setHours(0, 0, 0, 0);
        const paramDate = new Date(date).setHours(0, 0, 0, 0);
        return paramDate < today ? false : true;
    })
    .withMessage('Cannot select past date')
    .custom((date, { req }) => {
        const departureDate = new Date(req.query.departure).setHours(0, 0, 0, 0);
        const paramDate = new Date(date).setHours(0, 0, 0, 0);
        return paramDate < departureDate ? false : true;
    })
    .withMessage('Return date cannot be less than departure date!!')
);

module.exports ={
    validationArray:validationArray,
    returnTicketValidation:returnTicketValidation
}