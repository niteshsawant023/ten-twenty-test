# Javascript Developer Test project

## Steps to deploy code on localhoot

* Clone this repo
* run `npm install` to install dependancies
* run `npm start` to start the server.
* Access website at `http://localhost:3001`

## Notes

* I have used `lowdb` as database ,Its a lightweigt json based database library
* *operationalDays* was not clear to me ,so I have not used it in any query.
 
## Changelog

* Fixed glitch in data display
* Date is shown in human friendly format.
* Changed return flight search logic
* Refactored the code.
* I have tested return for search for date
    * 14/09/2018-15/09/2018
    * 16/09/2018-18/09/2018
